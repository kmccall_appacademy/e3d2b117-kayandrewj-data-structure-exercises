def range(arr)
  arr.sort!
  arr.last-arr.first
end

def in_order?(arr)
  arr == arr.sort
end

def num_vowels(str)
  str.downcase.scan(/[aeiou]/).count
end

def devowel(str)
  str.delete(str.scan(/[aeiou]/i).join)
end

def descending_digits(int)
  int.to_s.split("").sort.reverse
end

def repeating_letters?(str)
  str.downcase!
  str.length.times do |i|
    next if str.chars[i] != str.chars[i+1]
    return true
  end
  false
end

def to_phone_number(arr)
  "(#{arr[0..2].join}) #{arr[3..5].join}\-#{arr[6..9].join}"
end

def str_range(str)
  arr_from_str = str.split(",").map! { |num_str| num_str.to_i}
  range(arr_from_str)
end

def my_rotate(arr, offset=1)
  offset = offset % arr.length
  taken = arr.drop(offset)
  arr -= taken
  taken += arr
end
